//
//  DocumentsViewController.swift
//  InfoDev
//
//  Created by Newarpunk on 7/10/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit
import DLRadioButton

class DocumentsViewController: UIViewController {

    @IBOutlet weak var disclaimerLabel: UILabel!
    @IBOutlet weak var signatureTextField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var radioButton: DLRadioButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    // Error messages
    @IBOutlet weak var signatureError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var confirmPasswordError: UILabel!
    
    var datePicker = UIDatePicker()
    var pickedDate = ""
    var toolBar = UIToolbar()
    
    var isRadioButtonClicked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        currentDate()
    }
    
    // MARK: - Functions
    func setupViews() {
        signatureTextField.customTextField()
        passwordTextField.customTextField()
        confirmPasswordTextField.customTextField()
        submitButton.buttonRadius(radius: 20)
        previousButton.buttonRadius(radius: 20)
        previousButton.layer.borderWidth = 1
        previousButton.layer.borderColor = UIColor.darkGray.cgColor
        signatureError.isHidden = true
        passwordError.isHidden = true
        confirmPasswordError.isHidden = true
        disclaimerLabel.text = "I certify, under penalty of perjury, that I am 18 years old, not an employee of the Nepal Govermnet serving in the diplomatic mission or consulate and not in a student visa in USA. I further certify that the information provided on this form and any documents submitted by me are complete, true and correct. My name below proves that I intentionally completed and signed this form."
    }
    
    // For calendar date picking
    func callCalendar() {
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white
        
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .date
        
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height * 0.6, width: UIScreen.main.bounds.size.width, height: 270)
        self.view.addSubview(datePicker)
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: (UIScreen.main.bounds.size.height * 0.6) - 20, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"

        if let date = sender?.date {
            // print("Picked the date \(dateFormatter.string(from: date))")
            pickedDate = dateFormatter.string(from: date)
        }
    }
    
    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
        dateLabel.text = pickedDate
    }
    
    // For current Date
    func currentDate() {
        let dateFormatterDate = DateFormatter()
        dateFormatterDate.dateFormat = "MMM dd, yyyy"
        let date = Date()
        dateLabel.text = dateFormatterDate.string(from: date)
    }
    
    
    // For checking empty textfields
    func isTextFieldEmpty(_ tf: UITextField) -> Bool {
        return tf.text!.isEmpty != true
    }
    
    func checkingEmptyTextFields() {
        
        var isAllTextFieldNotEmpty: Bool = true
        
        if isTextFieldEmpty(signatureTextField) {
            signatureError.isHidden = true
        } else {
            signatureError.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(passwordTextField) {
            passwordError.isHidden = true
        } else {
            passwordError.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(confirmPasswordTextField) {
            confirmPasswordError.isHidden = true
        } else {
            confirmPasswordError.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
                
        if isAllTextFieldNotEmpty {
            if isRadioButtonClicked == false {
                let alert = UIAlertController(title: "Warning", message: "Please select terms & condition", preferredStyle: .alert)
                let okAlert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alert.addAction(okAlert)
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Success", message: "Submission successful", preferredStyle: .alert)
                let okAlert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alert.addAction(okAlert)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func checkPasswordMatch() {
        if passwordTextField.text != confirmPasswordTextField.text {
            let alert = UIAlertController(title: "Failed", message: "Password Doesn't Matched", preferredStyle: .alert)
            let okAlert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(okAlert)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Button Functions
    @IBAction func datePickerClicked(_ sender: UIButton) {
        callCalendar()
    }
    
    @IBAction func previousButtonClicked(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
    
    @IBAction func submitButtonClicked(_ sender: UIButton) {
        checkingEmptyTextFields()
    }

    @IBAction func signatureTextFieldChanged(_ sender: UITextField) {
        signatureError.isHidden = true
    }
    @IBAction func passwordTextFieldChanged(_ sender: UITextField) {
        passwordError.isHidden = true
    }
    @IBAction func confirmPasswordTextFieldChanged(_ sender: UITextField) {
        confirmPasswordError.isHidden = true
    }
    @IBAction func termsButtonClicked(_ sender: DLRadioButton) {
        checkPasswordMatch()
        isRadioButtonClicked = true
    }
}
