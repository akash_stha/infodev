//
//  EligiblilityViewController.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit
import DLRadioButton

class EligiblilityViewController: UIViewController {
    
    // Button declaration
    @IBOutlet weak var termButtonOne: DLRadioButton!
    @IBOutlet weak var termButtonTwo: DLRadioButton!
    @IBOutlet weak var termButtonThree: DLRadioButton!
    @IBOutlet weak var termButtonFour: DLRadioButton!
    
    @IBOutlet weak var continueButton: UIButton!
    
    // Terms label declaration
    @IBOutlet weak var termLabelOne: UILabel!
    @IBOutlet weak var termLabelTwo: UILabel!
    @IBOutlet weak var termLabelThree: UILabel!
    @IBOutlet weak var termLabelFour: UILabel!
    
    @IBOutlet weak var termsView: UIView!
    
    // Variable declarations
    let allTerms = TermsLabelArray()
    var termsButtonClicked: Bool = false
    
    var tabBarItem1 = UITabBarItem()
    var tabBarItem2 = UITabBarItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelAssign()
        customShape()
        disableOtherTabItem()
    }
    
    // MARK: - Label Value
    func labelAssign() {
        termLabelOne.text = allTerms.termsList[0].termsLabel
        termLabelTwo.text = allTerms.termsList[1].termsLabel
        termLabelThree.text = allTerms.termsList[2].termsLabel
        termLabelFour.text = allTerms.termsList[3].termsLabel
    }
    
    func customShape() {
        continueButton.layer.cornerRadius = continueButton.frame.size.height / 2
        termsView.layer.cornerRadius = 10
        termsView.layer.masksToBounds = true
        termButtonOne.iconSize = 20
        termButtonTwo.iconSize = 20
        termButtonThree.iconSize = 20
        termButtonFour.iconSize = 20
    }
    
    func disableOtherTabItem() {
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        if let tabArray = tabBarControllerItems {
            tabBarItem1 = tabArray[1]
            tabBarItem2 = tabArray[2]

            tabBarItem1.isEnabled = false
            tabBarItem2.isEnabled = false
        }
    }
    
    // MARK: - Button Functions
    @IBAction func termButtonIsClicked(_ sender: DLRadioButton) {
        print("button is clicked \(sender.tag)")
        termsButtonClicked = true
    }
    
    @IBAction func continueClicked(_ sender: UIButton) {
        if termsButtonClicked == false {
            ShowSnackBar.showSnackBarMessage(showMessage: "Please select one Eligiblity criteria")
        } else {
            self.tabBarController?.selectedIndex = 1
        }
    }
    

}
