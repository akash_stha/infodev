//
//  GeneralInformationViewController.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit
import DLRadioButton
import iOSDropDown

class GeneralInformationViewController: UIViewController {

    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var choosPhotoButton: UIButton!
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var termLabelOne: UILabel!
    @IBOutlet weak var termLabelTwo: UILabel!
    @IBOutlet weak var termLabelThree: UILabel!
    @IBOutlet weak var termLabelFour: UILabel!
    @IBOutlet weak var termLabelFive: UILabel!
    @IBOutlet weak var termLabelSix: UILabel!
    
    @IBOutlet weak var termButtonOne: DLRadioButton!
    @IBOutlet weak var termButtonTwo: DLRadioButton!
    @IBOutlet weak var termButtonThree: DLRadioButton!
    @IBOutlet weak var termButtonFour: DLRadioButton!
    @IBOutlet weak var termButtonFive: DLRadioButton!
    @IBOutlet weak var termButtonSix: DLRadioButton!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var professionTextField: UITextField!
    
    // Error message labels
    @IBOutlet weak var firstNameErrorMessage: UILabel!
    @IBOutlet weak var lastNameErrorMessage: UILabel!
    @IBOutlet weak var addressErrorMessage: UILabel!
    @IBOutlet weak var stateErrorMessage: UILabel!
    @IBOutlet weak var zipcodeErrorMessage: UILabel!
    @IBOutlet weak var mobileErrorMessage: UILabel!
    @IBOutlet weak var emailErrorMessage: UILabel!
    @IBOutlet weak var memberTypeErrorMessage: UILabel!
    @IBOutlet weak var photoErrorMessage: UILabel!
    @IBOutlet weak var dateformatErrorMessage: UILabel!
    @IBOutlet weak var identificationErrorMessage: UILabel!
    
    @IBOutlet weak var memberType: DropDown!
    @IBOutlet weak var countryName: DropDown!
    
    var identificationTermsList = TermsForIdentification()
    
    var isImageSelected: Bool = false
    var isRadioButtonClicked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customButtonShape()
        assignTermsLabel()
        customTextFields()
        dropDownMenu()
        hideAllErrorLabels()
    }
    
    @IBAction func choosePhotoClicked(_ sender: UIButton) {
        isImageSelected = true
        imagePicker()
    }
    
    @IBAction func previousClicked(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        textFieldValidation()
    }

    @IBAction func termsButtonClicked(_ sender: DLRadioButton) {
        isRadioButtonClicked = true
        identificationErrorMessage.isHidden = true
    }
    
    // MARK: - TextField Changed Functions
    @IBAction func firstNameTextFieldChanged(_ sender: UITextField) {
        firstNameErrorMessage.isHidden = true
    }
    @IBAction func lastNameTextFieldChanged(_ sender: UITextField) {
        lastNameErrorMessage.isHidden = true
    }
    @IBAction func dobTextFieldChanged(_ sender: UITextField) {
        dateformatErrorMessage.isHidden = true
    }
    @IBAction func addressTextFieldChanged(_ sender: UITextField) {
        addressErrorMessage.isHidden = true
    }
    @IBAction func stateTextFieldChanged(_ sender: UITextField) {
        stateErrorMessage.isHidden = true
    }
    @IBAction func zipcodeTextFieldChanged(_ sender: UITextField) {
        zipcodeErrorMessage.isHidden = true
    }
    @IBAction func mobileTextFieldChanged(_ sender: UITextField) {
        mobileErrorMessage.isHidden = true
    }
    @IBAction func emailTextFieldChanged(_ sender: UITextField) {
        emailErrorMessage.isHidden = true
    }
    
    
    // MARK: - Functions
    func hideAllErrorLabels() {
        firstNameErrorMessage.isHidden = true
        lastNameErrorMessage.isHidden = true
        addressErrorMessage.isHidden = true
        stateErrorMessage.isHidden = true
        zipcodeErrorMessage.isHidden = true
        mobileErrorMessage.isHidden = true
        emailErrorMessage.isHidden = true
        dateformatErrorMessage.isHidden = true
        photoErrorMessage.isHidden = true
        memberTypeErrorMessage.isHidden = true
        identificationErrorMessage.isHidden = true
    }
    
    func customButtonShape() {
        choosPhotoButton.buttonRadius(radius: 6)
        nextButton.buttonRadius(radius: 20)
        previousButton.buttonRadius(radius: 20)
        previousButton.layer.borderWidth = 1
        previousButton.layer.borderColor = UIColor.darkGray.cgColor
        termButtonOne.iconSize = 20
        termButtonTwo.iconSize = 20
        termButtonThree.iconSize = 20
        termButtonFour.iconSize = 20
        termButtonFive.iconSize = 20
        termButtonSix.iconSize = 20
    }
    
    func assignTermsLabel() {
        termLabelOne.text = identificationTermsList.identificationTerms[0].termsLabel
        termLabelTwo.text  = identificationTermsList.identificationTerms[1].termsLabel
        termLabelThree.text = identificationTermsList.identificationTerms[2].termsLabel
        termLabelFour.text  = identificationTermsList.identificationTerms[3].termsLabel
        termLabelFive.text  = identificationTermsList.identificationTerms[4].termsLabel
        termLabelSix.text = identificationTermsList.identificationTerms[5].termsLabel
    }
    
    func customTextFields() {
        firstNameTextField.customTextField()
        middleNameTextField.customTextField()
        lastNameTextField.customTextField()
        dobTextField.customTextField()
        addressTextField.customTextField()
        stateTextField.customTextField()
        cityTextField.customTextField()
        zipCodeTextField.customTextField()
        mobileNumberTextField.customTextField()
        emailTextField.customTextField()
        professionTextField.customTextField()
    }
    
    // For checking empty textfields
    func isTextFieldEmpty(_ tf: UITextField) -> Bool {
        return tf.text!.isEmpty != true
    }
        
    func textFieldValidation() {
//        if firstNameTextField.text?.isEmpty == true || lastNameTextField.text?.isEmpty == true || addressTextField.text?.isEmpty == true || stateTextField.text?.isEmpty == true || zipCodeTextField.text?.isEmpty == true || mobileNumberTextField.text?.isEmpty == true || emailTextField.text?.isEmpty == true {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please enter the *marked details")
//        } else if mobileNumberTextField.text!.isValidateNumber == false {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please enter phone details")
//        } else if emailTextField.text!.isValidEmail == false {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please enter email details")
//        } else  if memberType.selectedIndex == nil {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please select the member type")
//        } else if isImageSelected == false {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please select the image")
//        } else if isRadioButtonClicked == false {
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please select the membership identification details")
//        }else if dobTextField.text?.isBirthDateValidate == false{
//            ShowSnackBar.showSnackBarMessage(showMessage: "Please enter date in MM-DD-YYYY format")
//        } else {
//            self.tabBarController?.selectedIndex = 2
//        }
        
        var isAllTextFieldNotEmpty: Bool = true
        
        if isTextFieldEmpty(firstNameTextField) {
            firstNameErrorMessage.isHidden = true
        } else {
            firstNameErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(lastNameTextField) {
            lastNameErrorMessage.isHidden = true
        } else {
            lastNameErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(addressTextField) {
            addressErrorMessage.isHidden = true
        } else {
            addressErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(stateTextField) {
            stateErrorMessage.isHidden = true
        } else {
            stateErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(zipCodeTextField) {
            zipcodeErrorMessage.isHidden = true
        } else {
            zipcodeErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(mobileNumberTextField) {
            mobileErrorMessage.isHidden = true
            if mobileNumberTextField.text!.isValidateNumber == false {
                mobileErrorMessage.isHidden = false
                mobileErrorMessage.text = "*Please enter valid number"
                isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
            }
        } else {
            mobileErrorMessage.text = "*Please enter mobile number"
            mobileErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(emailTextField) {
            emailErrorMessage.isHidden = true
            if emailTextField.text!.isValidEmail == false {
                emailErrorMessage.text = "*Please enter valid email"
                emailErrorMessage.isHidden = false
                isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
            }
        } else {
            emailErrorMessage.text = "*Please enter email"
            emailErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
                
        if isImageSelected {
            photoErrorMessage.isHidden = true
        } else {
            photoErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isRadioButtonClicked {
            identificationErrorMessage.isHidden = true
        } else {
            identificationErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if memberType.selectedIndex != nil {
            memberTypeErrorMessage.isHidden = true
        } else {
            memberTypeErrorMessage.isHidden = false
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isTextFieldEmpty(dobTextField) {
            dateformatErrorMessage.isHidden = true
            if dobTextField.text?.isBirthDateValidate == false {
                dateformatErrorMessage.isHidden = false
                dateformatErrorMessage.text = "*Please enter date in MM-DD-YYYY format"
                isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
            }
        } else {
            dateformatErrorMessage.isHidden = false
            dateformatErrorMessage.text = "*Please enter D.O.B."
            isAllTextFieldNotEmpty = isAllTextFieldNotEmpty && false
        }
        
        if isAllTextFieldNotEmpty {
            self.tabBarController?.selectedIndex = 2
        }
        
    }
    
    func imagePicker() {
        let imageController = UIImagePickerController()
        
        let alertBox = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAlert = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            imageController.sourceType = .camera
            imageController.delegate = self
            self.present(imageController, animated: true, completion: nil)
        }
        let photoLibraryAlert = UIAlertAction(title: "PhotoLibrary", style: .default) { (UIAlertAction) in
            imageController.delegate = self
            imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imageController, animated: true, completion: nil)
        }
        let cancelAlert = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertBox.addAction(cameraAlert)
        alertBox.addAction(photoLibraryAlert)
        alertBox.addAction(cancelAlert)
        self.present(alertBox, animated: true, completion: nil)
        
    }
    
    func dropDownMenu() {
        // For County name
        countryName.arrowSize = 20
        countryName.arrowColor = .darkGray
        countryName.selectedRowColor = .clear
        countryName.listHeight = 180
        countryName.rowHeight = 45
        countryName.checkMarkEnabled = true

        var countryArray = [String]()
        countryArray.append("United States")
        countryName.optionArray = countryArray
        
        // For membertype
        memberType.arrowSize = 20
        memberType.arrowColor = .darkGray
        memberType.selectedRowColor = .clear
        memberType.listHeight = 180
        memberType.rowHeight = 45
        memberType.checkMarkEnabled = true
        
        memberType.didSelect { (selectedText, index, id) in
            self.memberTypeErrorMessage.isHidden = true
        }
        
        var listArray = [String]()
        listArray.append("Premium")
        listArray.append("Classic")
        listArray.append("Guest")
        memberType.optionArray = listArray
    }

}

extension GeneralInformationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        photoErrorMessage.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
}
