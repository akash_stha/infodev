//
//  TermsLabelModel.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class TermLabelModel {
    
    var termsLabel: String
    
    init(termsLabel: String) {
        self.termsLabel = termsLabel
    }
    
}
