//
//  TermsLabelArray.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class TermsLabelArray {
    
    var termsList = [TermLabelModel]()
    
    init() {
        termsList.append(TermLabelModel(termsLabel: "I am a Nepali citizen and have lived out of Nepal (other than SAARC countires) more than 2 years and currently living in the USA. I am not a Permanent Resident (green card holder) of the USA"))
        termsList.append(TermLabelModel(termsLabel: "I am Nepali citizen and a Permanent Resident (green card holder) of the USA"))
        termsList.append(TermLabelModel(termsLabel: "I am a US citizen but I am a PNO [People of Nepali Origin = I was or my parents or my grand parents are/were citizen(s) of Nepal]"))
        termsList.append(TermLabelModel(termsLabel: "I am a foreign (Non-Nepali, Non-USA and Non-SAARC country) citizen but I am a PNO [People of Nepali Origin = I was or my parents or my grand parents are/were citizen(s) of Nepal]"))
    }
    
}


class TermsForIdentification {
    var identificationTerms = [TermLabelModel]()
    
    init() {
        identificationTerms.append(TermLabelModel(termsLabel: "Valid driver license issued by US State"))
        identificationTerms.append(TermLabelModel(termsLabel: "Passport issued by Goverment of USA"))
        identificationTerms.append(TermLabelModel(termsLabel: "Permanent Resident Card issued by Goverment of USA"))
        identificationTerms.append(TermLabelModel(termsLabel: "Identification card issued by US State Goverment"))
        identificationTerms.append(TermLabelModel(termsLabel: "Birth certificate issued by US Goverment"))
        identificationTerms.append(TermLabelModel(termsLabel: "Passport issued by Goverment of Nepal and proof of ........"))
    }
}
