//
//  ShowSnackBar.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

import Foundation
import MaterialComponents.MaterialSnackbar

class ShowSnackBar {
    public static func showSnackBarMessage(showMessage: String) {
        MDCSnackbarMessageView.appearance().snackbarMessageViewBackgroundColor = .blue
        let message = MDCSnackbarMessage()
        message.text = showMessage
        MDCSnackbarManager.show(message)
    }
}
