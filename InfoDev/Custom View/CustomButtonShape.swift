//
//  CustomButtonShape.swift
//  InfoDev
//
//  Created by Newarpunk on 7/8/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

extension UIButton {
    
    func buttonRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
}
