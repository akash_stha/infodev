//
//  Preferences.swift
//  InfoDev
//
//  Created by Newarpunk on 7/10/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import UIKit

class Preferences {
    func isTextFieldEmpty(_ tf: UITextField) -> Bool {
        return tf.text!.isEmpty != true
    }
}
